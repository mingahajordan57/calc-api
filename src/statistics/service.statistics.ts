const calculateMean = (numbers: number[]): number => numbers.reduce((prev, curr) => prev + curr) / numbers.length;

const calculateMedian = (numbers: number[]): number => {
    const sortedNumbers = numbers.sort((a, b) => a - b);
    const mid = Math.floor(sortedNumbers.length / 2);
    return sortedNumbers.length % 2 !== 0 ? sortedNumbers[mid] : (sortedNumbers[mid - 1] + sortedNumbers[mid]) / 2;
}

const calculateVariance = (numbers: number[]): number => numbers.reduce((sum, value) => sum + Math.pow(value - calculateMean(numbers), 2), 0) / numbers.length;

const calculateStandardDeviation = (numbers: number[]): number => Math.sqrt(calculateVariance(numbers));

export {calculateMean, calculateMedian, calculateStandardDeviation};
